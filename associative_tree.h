#pragma once

#include <unordered_map>

template <typename T>
class associative_tree
{
public:
	typedef std::unordered_map<T, associative_tree> ChildMap;

	associative_tree(const T& key) : _key(key) {}
	virtual ~associative_tree(void) {}

	T key() const { return _key; }

	typename ChildMap::const_iterator begin() const { return children.begin(); }
	typename ChildMap::const_iterator end() const { return children.end(); }
	typename ChildMap::const_iterator operator[](const T& key) const { return children[key]; }
	std::size_t size() const { return children.size(); }

	void insert(const T& key, const associative_tree& child) { children.insert(std::make_pair(key, child)); }

private:
	std::string _key;
	ChildMap children;
};