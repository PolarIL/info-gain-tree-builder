#include "info_gain_queue.h"

#include <iostream>

double log2(double x)
{
	return log(x) / log(2);
}

info_gain_queue::info_gain_queue(record_database& db, const string& field_to_decide) : db(db), field_to_decide(field_to_decide), total_entropy(0)
{
	using std::make_pair;

	total_entropy = calculate_entropy(db, field_to_decide);

	// Calculate the info gain for each available field.
	for (const auto& field : db.get_fields())
	{
		if (field == field_to_decide)
			continue;

		field_info_gains.push(make_pair(calculate_info_gain(db, field, field_to_decide), field));
	}
}

double info_gain_queue::calculate_entropy(record_database& db, const string& field)
{
	std::vector<double> value_counts;

	// Get a count for each of the possible values of the field.
	for (const auto& value : db.get_field_values(field))
		value_counts.push_back(db.count_records_where(field, value));

	// Calculate how many records the field appears in.
	double field_total = 0;
	for (const auto& count : value_counts)
		field_total += count;

	// Calculate the total entropy for the field.
	double entropy = 0;
	for (const auto& count: value_counts)
		entropy -= (count / field_total) * log2(count / field_total);

	return entropy;
}

double info_gain_queue::calculate_info_gain(record_database& db, const string& field, const string& field_to_decide)
{
	double info_gain = total_entropy;
	
	for (const auto& value : db.get_field_values(field))
	{
		record_database filtered_db = record_database::filter_from(db, field, value);
		info_gain -= calculate_entropy(filtered_db, field_to_decide) * (db.count_records_where(field, value) / db.size());
	}

	return info_gain;
}

info_gain_queue::~info_gain_queue(void)
{
}
