#include <unordered_set>
#include <iostream>
#include <cmath>

#include "record_database.h"
#include "info_gain_queue.h"
#include "associative_tree.h"

typedef associative_tree<std::string> decision_tree;

void load_test_samples(record_database& db)
{
	using std::make_pair;

	Record r1;
	r1.insert(make_pair("Size", "Large"));
	r1.insert(make_pair("Occupied", "Highly"));
	r1.insert(make_pair("Price", "Expensive"));
	r1.insert(make_pair("Music", "Loud"));
	r1.insert(make_pair("Location", "Rotchild Blvd."));
	r1.insert(make_pair("VIP", "No"));
	r1.insert(make_pair("Favorite Beer", "No"));
	r1.insert(make_pair("Enjoy", "No"));

	Record r2;
	r2.insert(make_pair("Size", "Large"));
	r2.insert(make_pair("Occupied", "Highly"));
	r2.insert(make_pair("Price", "Expensive"));
	r2.insert(make_pair("Music", "Loud"));
	r2.insert(make_pair("Location", "Tel-Aviv Seaport"));
	r2.insert(make_pair("VIP", "Yes"));
	r2.insert(make_pair("Favorite Beer", "No"));
	r2.insert(make_pair("Enjoy", "Yes"));

	Record r3;
	r3.insert(make_pair("Size", "Large"));
	r3.insert(make_pair("Occupied", "Yes"));
	r3.insert(make_pair("Price", "Normal"));
	r3.insert(make_pair("Music", "Quiet"));
	r3.insert(make_pair("Location", "Tel-Aviv Seaport"));
	r3.insert(make_pair("VIP", "No"));
	r3.insert(make_pair("Favorite Beer", "Yes"));
	r3.insert(make_pair("Enjoy", "Yes"));

	Record r4;
	r4.insert(make_pair("Size", "Medium"));
	r4.insert(make_pair("Occupied", "Yes"));
	r4.insert(make_pair("Price", "Expensive"));
	r4.insert(make_pair("Music", "Quiet"));
	r4.insert(make_pair("Location", "King George St."));
	r4.insert(make_pair("VIP", "No"));
	r4.insert(make_pair("Favorite Beer", "No"));
	r4.insert(make_pair("Enjoy", "No"));

	Record r5;
	r5.insert(make_pair("Size", "Large"));
	r5.insert(make_pair("Occupied", "Yes"));
	r5.insert(make_pair("Price", "Expensive"));
	r5.insert(make_pair("Music", "Quiet"));
	r5.insert(make_pair("Location", "King George St."));
	r5.insert(make_pair("VIP", "Yes"));
	r5.insert(make_pair("Favorite Beer", "Yes"));
	r5.insert(make_pair("Enjoy", "Yes"));

	Record r6;
	r6.insert(make_pair("Size", "Small"));
	r6.insert(make_pair("Occupied", "Yes"));
	r6.insert(make_pair("Price", "Normal"));
	r6.insert(make_pair("Music", "Quiet"));
	r6.insert(make_pair("Location", "Hayarkon St."));
	r6.insert(make_pair("VIP", "No"));
	r6.insert(make_pair("Favorite Beer", "No"));
	r6.insert(make_pair("Enjoy", "Yes"));

	Record r7;
	r7.insert(make_pair("Size", "Large"));
	r7.insert(make_pair("Occupied", "No"));
	r7.insert(make_pair("Price", "Normal"));
	r7.insert(make_pair("Music", "Quiet"));
	r7.insert(make_pair("Location", "Hayarkon St."));
	r7.insert(make_pair("VIP", "No"));
	r7.insert(make_pair("Favorite Beer", "No"));
	r7.insert(make_pair("Enjoy", "No"));

	Record r8;
	r8.insert(make_pair("Size", "Small"));
	r8.insert(make_pair("Occupied", "Yes"));
	r8.insert(make_pair("Price", "Cheap"));
	r8.insert(make_pair("Music", "Loud"));
	r8.insert(make_pair("Location", "Florentin"));
	r8.insert(make_pair("VIP", "No"));
	r8.insert(make_pair("Favorite Beer", "No"));
	r8.insert(make_pair("Enjoy", "Yes"));

	Record r9;
	r9.insert(make_pair("Size", "Medium"));
	r9.insert(make_pair("Occupied", "Highly"));
	r9.insert(make_pair("Price", "Expensive"));
	r9.insert(make_pair("Music", "Loud"));
	r9.insert(make_pair("Location", "Tel-Aviv Seaport"));
	r9.insert(make_pair("VIP", "Yes"));
	r9.insert(make_pair("Favorite Beer", "Yes"));
	r9.insert(make_pair("Enjoy", "Yes"));

	Record r10;
	r10.insert(make_pair("Size", "Medium"));
	r10.insert(make_pair("Occupied", "No"));
	r10.insert(make_pair("Price", "Cheap"));
	r10.insert(make_pair("Music", "Quiet"));
	r10.insert(make_pair("Location", "Tel-Aviv Seaport"));
	r10.insert(make_pair("VIP", "No"));
	r10.insert(make_pair("Favorite Beer", "No"));
	r10.insert(make_pair("Enjoy", "No"));

	Record r11;
	r11.insert(make_pair("Size", "Large"));
	r11.insert(make_pair("Occupied", "Yes"));
	r11.insert(make_pair("Price", "Cheap"));
	r11.insert(make_pair("Music", "Loud"));
	r11.insert(make_pair("Location", "Rotchild Blvd."));
	r11.insert(make_pair("VIP", "No"));
	r11.insert(make_pair("Favorite Beer", "Yes"));
	r11.insert(make_pair("Enjoy", "No"));

	Record r12;
	r12.insert(make_pair("Size", "Large"));
	r12.insert(make_pair("Occupied", "No"));
	r12.insert(make_pair("Price", "Cheap"));
	r12.insert(make_pair("Music", "Quiet"));
	r12.insert(make_pair("Location", "Rotchild Blvd."));
	r12.insert(make_pair("VIP", "Yes"));
	r12.insert(make_pair("Favorite Beer", "Yes"));
	r12.insert(make_pair("Enjoy", "No"));

	Record r13;
	r13.insert(make_pair("Size", "Medium"));
	r13.insert(make_pair("Occupied", "Yes"));
	r13.insert(make_pair("Price", "Expensive"));
	r13.insert(make_pair("Music", "Quiet"));
	r13.insert(make_pair("Location", "Florentin"));
	r13.insert(make_pair("VIP", "No"));
	r13.insert(make_pair("Favorite Beer", "Yes"));
	r13.insert(make_pair("Enjoy", "Yes"));

	Record r14;
	r14.insert(make_pair("Size", "Medium"));
	r14.insert(make_pair("Occupied", "Highly"));
	r14.insert(make_pair("Price", "Normal"));
	r14.insert(make_pair("Music", "Loud"));
	r14.insert(make_pair("Location", "Florentin"));
	r14.insert(make_pair("VIP", "Yes"));
	r14.insert(make_pair("Favorite Beer", "Yes"));
	r14.insert(make_pair("Enjoy", "Yes"));

	Record r15;
	r15.insert(make_pair("Size", "Large"));
	r15.insert(make_pair("Occupied", "Yes"));
	r15.insert(make_pair("Price", "Normal"));
	r15.insert(make_pair("Music", "Loud"));
	r15.insert(make_pair("Location", "Hayarkon St."));
	r15.insert(make_pair("VIP", "No"));
	r15.insert(make_pair("Favorite Beer", "Yes"));
	r15.insert(make_pair("Enjoy", "Yes"));

	Record r16;
	r16.insert(make_pair("Size", "Small"));
	r16.insert(make_pair("Occupied", "Highly"));
	r16.insert(make_pair("Price", "Normal"));
	r16.insert(make_pair("Music", "Quiet"));
	r16.insert(make_pair("Location", "King George St."));
	r16.insert(make_pair("VIP", "No"));
	r16.insert(make_pair("Favorite Beer", "No"));
	r16.insert(make_pair("Enjoy", "No"));

	Record r17;
	r17.insert(make_pair("Size", "Large"));
	r17.insert(make_pair("Occupied", "Highly"));
	r17.insert(make_pair("Price", "Cheap"));
	r17.insert(make_pair("Music", "Loud"));
	r17.insert(make_pair("Location", "Tel-Aviv Seaport"));
	r17.insert(make_pair("VIP", "No"));
	r17.insert(make_pair("Favorite Beer", "Yes"));
	r17.insert(make_pair("Enjoy", "Yes"));

	Record r18;
	r18.insert(make_pair("Size", "Small"));
	r18.insert(make_pair("Occupied", "No"));
	r18.insert(make_pair("Price", "Normal"));
	r18.insert(make_pair("Music", "Quiet"));
	r18.insert(make_pair("Location", "Tel-Aviv Seaport"));
	r18.insert(make_pair("VIP", "No"));
	r18.insert(make_pair("Favorite Beer", "No"));
	r18.insert(make_pair("Enjoy", "No"));

	Record r19;
	r19.insert(make_pair("Size", "Medium"));
	r19.insert(make_pair("Occupied", "No"));
	r19.insert(make_pair("Price", "Expensive"));
	r19.insert(make_pair("Music", "Loud"));
	r19.insert(make_pair("Location", "Florentin"));
	r19.insert(make_pair("VIP", "No"));
	r19.insert(make_pair("Favorite Beer", "No"));
	r19.insert(make_pair("Enjoy", "No"));

	Record r20;
	r20.insert(make_pair("Size", "Medium"));
	r20.insert(make_pair("Occupied", "Yes"));
	r20.insert(make_pair("Price", "Normal"));
	r20.insert(make_pair("Music", "Quiet"));
	r20.insert(make_pair("Location", "Rotchild Blvd."));
	r20.insert(make_pair("VIP", "No"));
	r20.insert(make_pair("Favorite Beer", "No"));
	r20.insert(make_pair("Enjoy", "Yes"));

	Record r21;
	r21.insert(make_pair("Size", "Medium"));
	r21.insert(make_pair("Occupied", "No"));
	r21.insert(make_pair("Price", "Normal"));
	r21.insert(make_pair("Music", "Quiet"));
	r21.insert(make_pair("Location", "Tel-Aviv Seaport"));
	r21.insert(make_pair("VIP", "No"));
	r21.insert(make_pair("Favorite Beer", "No"));
	r21.insert(make_pair("Enjoy", "Yes"));

	db.insert(r1);
	db.insert(r2);
	db.insert(r3);
	db.insert(r4);
	db.insert(r5);
	db.insert(r6);
	db.insert(r7);
	db.insert(r8);
	db.insert(r9);
	db.insert(r10);
	db.insert(r11);
	db.insert(r12);
	db.insert(r13);
	db.insert(r14);
	db.insert(r15);
	db.insert(r16);
	db.insert(r17);
	db.insert(r18);
	db.insert(r19);
	db.insert(r20);
	db.insert(r21);
}

decision_tree build_ig_decision_tree(record_database& db, const std::string& field_to_decide)
{
	info_gain_queue gain(db, field_to_decide);
	double top_gain       = gain.top().first;
	std::string top_field = gain.top().second;
	double entropy        = gain.entropy();

	// If there is no uncertainty or there are no fields left to consider,
	// just return a node with the majority decision.
	if (entropy == 0.0 || db.get_fields().size() == 0)
		return decision_tree(db.get_majority_value(field_to_decide));

	decision_tree root(top_field);

	// For each possible value of top_field, build a subtree while considering other fields
	// for the set of records where top_field == value.
	for (const auto& value : db.get_field_values(top_field))
		root.insert(value, build_ig_decision_tree(record_database::filter_from(db, top_field, value), field_to_decide));

	return root;
}

void print_decision_tree(const decision_tree& tree, std::size_t depth)
{
	string prefix(depth, '\t');

	// Final decision node.
	if (tree.size() == 0)
		std::cout << prefix << "decision(" << tree.key() << ");" << std::endl;

	for (const auto& child : tree)
	{
		std::cout << prefix << "for " << tree.key() << " = " << child.first << (child.second.size() > 1 ? " {" : "") << std::endl;
		print_decision_tree(child.second, depth + 1);
		if (child.second.size() > 1)
			std::cout << prefix << "}" << std::endl;
	}
}

int main(int, char**)
{
	record_database db;
	
	load_test_samples(db);

	decision_tree tree = build_ig_decision_tree(db, "Enjoy");

	print_decision_tree(tree, 0);

	return 0;
}