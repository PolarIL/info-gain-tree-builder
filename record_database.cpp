#include "record_database.h"

#include <iostream>
#include <string>
#include <queue>

record_database::record_database(void)
{
}

record_database::~record_database(void)
{
}

void record_database::insert(const Record& r)
{
	// Insert fields into fields map.
	intern_fields(r);

	// Store the sample.
	samples.push_back(r);
}

record_database record_database::filter_from(const record_database& db, const string& field, const string& value)
{
	record_database ret;

	for (auto record : db)
		if (record[field] == value)
		{
			record.erase(field);
			ret.insert(record);
		}

	return ret;
}

unsigned int record_database::count_records_where(const string& field, const string& value) const
{
	unsigned int count = 0;

	for (auto record : samples)
		if (record[field] == value)
			++count;

	return count;
}

string record_database::get_majority_value(const string& field) const
{
	using std::priority_queue;
	using std::pair;
	using std::make_pair;
	using std::size_t;
	using std::string;
	
	priority_queue<pair<size_t, string>> q;

	for (const auto& value : get_field_values(field))
		q.push(make_pair(count_records_where(field, value), value));

	return q.top().second;
}

void record_database::intern_fields(const Record& record)
{
	for (const auto& field : record)
	{
		auto& field_list = fields[field.first];

		field_list.insert(field.second);
	}
}

FieldList record_database::get_fields() const
{
	FieldList list;

	for (const auto& field : fields)
		list.push_back(field.first);

	return list;

}

ValueSet record_database::get_field_values(const string& field) const
{
	return fields.at(field);
}

namespace std
{
	ostream& operator <<(ostream& o, const Field& field)
	{
		o << '"' << field.first << '"' << " = " << '"' << field.second << '"';

		return o;
	}

	ostream& operator <<(ostream& o, const Record& record)
	{
		for (const auto& field : record)
			std::cout << field << ',' << std::endl;

		return o;
	}
	
}