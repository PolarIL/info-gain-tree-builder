#pragma once

#include <string>
#include <queue>

#include "record_database.h"

using std::priority_queue;
using std::string;
using std::size_t;
using std::pair;

class info_gain_queue
{
public:
	info_gain_queue(record_database& db, const string& field_to_decide);
	virtual ~info_gain_queue(void);

	pair<double, string> top() const { return field_info_gains.top(); }
	bool empty() const { return field_info_gains.empty(); }
	size_t size() const { return field_info_gains.size(); }
	void pop() { field_info_gains.pop(); }

	double entropy() { return total_entropy; }

private:
	record_database& db;
	const string field_to_decide;

	double total_entropy;
	priority_queue<pair<double, string>> field_info_gains;

	double calculate_entropy(record_database& db, const string& field_to_decide);
	double calculate_info_gain(record_database& db, const string& field, const string& field_to_decide);
};

