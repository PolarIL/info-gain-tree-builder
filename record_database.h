#pragma once

#include <unordered_map>
#include <unordered_set>
#include <string>

using std::string;

typedef std::pair<string, string> Field;
typedef std::unordered_map<string, std::string> Record;
typedef std::vector<Record> RecordSet;
typedef std::vector<string> FieldList;
typedef std::unordered_set<string> ValueSet;

class record_database
{
public:
	record_database(void);
	virtual ~record_database(void);

	RecordSet::const_iterator begin() const { return samples.begin(); }
	RecordSet::const_iterator end() const { return samples.end(); }
	size_t size() const { return samples.size(); }

	void insert(const Record& r);

	static record_database filter_from(const record_database& other, const string& field, const string& value);
	unsigned int count_records_where(const string& field, const string& value) const;
	string get_majority_value(const string& field) const;

	FieldList get_fields() const;
	ValueSet get_field_values(const string& field) const;

private:
	void record_database::intern_fields(const Record& record);

	RecordSet samples;
	std::unordered_map<string, ValueSet> fields;
};

namespace std
{
	ostream& operator <<(ostream& o, const Field& field);
	ostream& operator <<(ostream& o, const Record& record);
}